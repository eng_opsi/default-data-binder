package it.eng.digitalenabler.sdk.databinder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.http.entity.StringEntity;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;

import it.eng.digitalenabler.sdk.databinder.model.DataBinder;








@SuppressWarnings("deprecation")
public class DefaultDataBinder implements DataBinder {
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Override
	public void init() {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.setDateFormat(new ISO8601DateFormat());
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		//Support for JDK8 Types
		objectMapper.registerModule(new Jdk8Module());
	}
	
	@Override
	public <T> String toJson(T entity) {
		String out = "{}";
		try { out = objectMapper.writeValueAsString(entity); }
		catch (Exception e) {
			e.printStackTrace();
		}
		return out;
	}

	@Override
	public <T> T toEntity(String json, Class<T> clazz) {
		Optional<T> out = Optional.empty();
		try {
				StringEntity entity= new StringEntity(json, "UTF-8");
				String result = new BufferedReader(new InputStreamReader(entity.getContent()))
						  .lines().collect(Collectors.joining("\n"));
			//Do the deserialization with Jackson
			out = Optional.of(objectMapper.readValue(result.getBytes(), clazz)); }
		catch (Exception e) {
			//In case of errors in the Deserialization returns with the empty Optional entity
			e.printStackTrace();
		}
		
		//If the Optional has a value, returns it; otherwise returns null
		return out.orElse(null);
	}


}
